vim.o.guifont = "JetBrainsMono Nerd Font:h9" -- text below applies for VimScript
vim.g.neovide_remember_window_size = true
vim.g.neovide_cursor_animation_length = 0.13
vim.g.neovide_cursor_animate_in_insert_mode = true
vim.g.neovide_cursor_vfx_mode = "sonicboom"
vim.g.neovide_transparency = 0.6
vim.g.neovide_hide_mouse_when_typing = true
